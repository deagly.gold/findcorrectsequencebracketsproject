package ru.eternaljunior.findcorrectsequencebracketsproject;

import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        System.out.println(findCorrectSequenceBrackets(")()())"));
    }

    public static String findCorrectSequenceBrackets(String s) {
        String maxLengthString = "";

        for (int i = 0; i <= s.length(); i++) {
            for (int j = i; j <= s.length(); j++) {
                String substring = s.substring(i, j);
                if(findCorrectBrackets(substring)) {
                    if(substring.length() > maxLengthString.length()) {
                        maxLengthString = substring;
                    }
                }
            }
        }
        if(maxLengthString.length() > 0) {
            return maxLengthString.length() + " - " + maxLengthString;
        } else {
            return "0";
        }
    }

    public static boolean findCorrectBrackets(String s) {
        char[] chars = s.toCharArray();
        Stack<Character> stack = new Stack<>();
        for (char aChar : chars) {
            if (((Character) aChar).equals('(')) {
                stack.push(aChar);
            } else {
                if (!stack.isEmpty()) {
                    stack.pop();
                } else {
                    stack.push(aChar);
                }
            }
        }
        return stack.isEmpty();
    }

}